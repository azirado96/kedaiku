<?php

namespace App\Controllers;

class Gambar extends BaseController
{
	function __construct(){
		
		$this->session = \Config\Services::session();

	}

	public function index()
	{
		// $users = $userModel->findAll();
		$gambar_model = new \App\Models\GambarModel();
		// $gambar = $gambar_model->paginate(3);

		// foreach ($gambar as $g) {
		// 	print_r($g);
		// 	echo "<br>";
		// }
		// die();
		//dd( $gambar);

		// $model = new \App\Models\UserModel();

        $data = [
            'gambar' => $gambar_model->orderBy('id', 'desc')->paginate(3),
            'pager' => $gambar_model->pager,
        ];

        // echo view('users/index', $data);

		// return view('admin/listing', ['gambar' => $gambar ]);
		return view('admin/listing', $data );

	}

	function edit($id){
		helper('form');
		$gambar_model = new \App\Models\GambarModel();
		$gambar = $gambar_model->find($id);

		return view('admin/edit', ['gambar' => $gambar]);

	}

	function save_edit($id){
		$gambar_model = new \App\Models\GambarModel();

		$data = [
			'nama' => $this->request->getPost('nama'),
			'keterangan' => $this->request->getPost('keterangan')
		];

		$file = $this->request->getFile('nama_fail');

		// Grab the file by name given in HTML form
		if ($file)
		{
			// Generate a new secure name
			$nama_fail = $file->getRandomName();

			// Move the file to it's new home
			$file->move('img/', $nama_fail);

			$data['nama_fail'] = $nama_fail;
		}

		$gambar_model->update($id, $data);

		$_SESSION['success'] = true;
		$this->session->markAsFlashdata('success');

		return redirect()->to('/gambar/edit/'.$id);

	}

	function delete($id){

		$gambar_model = new \App\Models\GambarModel();
		$gambar_model->where('id', $id)->delete();

		$_SESSION['deleted'] = true;
		$this->session->markAsFlashdata('deleted');

		return redirect()->back();
	}

	function add(){
		helper('form');
		return view('admin/add');
	}

	//utk save data dr add new form
	function save_new(){
		
		$gambar_model = new \App\Models\GambarModel();

		// $nama = $this->request->getPost('nama');
		// $keterangan = $this->request->getPost('keterangan');

		$data = [
			'nama' => $this->request->getPost('nama'),
			'keterangan' => $this->request->getPost('keterangan')
		];

		$file = $this->request->getFile('nama_fail');

		//dd($files);

		// Grab the file by name given in HTML form
		if ($file)
		{
			// $file = $files->getFile('nama_fail');

			// Generate a new secure name
			$nama_fail = $file->getRandomName();

			// Move the file to it's new home
			$file->move('img/', $nama_fail);

			$data['nama_fail'] = $nama_fail;
		}

		$gambar_model->insert($data);

		$_SESSION['success'] = true;
		$this->session->markAsFlashdata('success');

		return redirect()->to('/gambar');

		//dd($keterangan);

		// echo "<h1>heloo save data</h1>";
	}

}
