<?php

namespace App\Controllers;

class Home extends BaseController
{
	public function index()
	{
		//cara1
		// $all_pekan = [];

		// $pekan = [
		// 	'nama' => 'Penang',
		// 	'gambar' => 'https://images.unsplash.com/photo-1585644198841-c350f0d9d05c?ixid=MXwxMjA3fDB8MHxzZWFyY2h8Mnx8cGVuYW5nfGVufDB8fDB8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60',
		// 	'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Officiis dolorum quo, soluta ullam deleniti tempora sint consequatur neque facere eligendi praesentium omnis libero, amet illum laudantium incidunt doloribus perspiciatis! Sit.'
		// ];

		// $all_pekan[] = $pekan;

		// $pekan = [
		// 	'nama' => 'Kedah',
		// 	'gambar' => 'https://images.unsplash.com/photo-1591881332597-525ce4e76817?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MjB8fGtlZGFofGVufDB8fDB8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60',
		// 	'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Officiis dolorum quo, soluta ullam deleniti tempora sint consequatur neque facere eligendi praesentium omnis libero, amet illum laudantium incidunt doloribus perspiciatis! Sit.'
		// ];

		// $all_pekan[] = $pekan;

		//cara2
		// $all_pekan = [
		// 	[
		// 		'nama' => 'Georgetown',
		// 		'gambar' => 'https://images.unsplash.com/photo-1585644198841-c350f0d9d05c?ixid=MXwxMjA3fDB8MHxzZWFyY2h8Mnx8cGVuYW5nfGVufDB8fDB8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60',
		// 		'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Officiis dolorum quo, soluta ullam deleniti tempora sint consequatur neque facere eligendi praesentium omnis libero, amet illum laudantium incidunt doloribus perspiciatis! Sit.'
		// 	],
		// 	[
		// 		'nama' => 'Batu Feringhi',
		// 		'gambar' => 'https://images.unsplash.com/photo-1452784444945-3f422708fe5e?ixid=MXwxMjA3fDB8MHxzZWFyY2h8OXx8YmF0dSUyMGZlcnJpbmdoaSUyMGJlYWNofGVufDB8fDB8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60',
		// 		'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Officiis dolorum quo, soluta ullam deleniti tempora sint consequatur neque facere eligendi praesentium omnis libero, amet illum laudantium incidunt doloribus perspiciatis! Sit.'
		// 	],
		// 	[
		// 		'nama' => 'Jelutong',
		// 		'gambar' => 'https://images.unsplash.com/photo-1612025710202-969463b1b834?ixid=MXwxMjA3fDB8MHxzZWFyY2h8NXx8cGVuYW5nfGVufDB8fDB8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60',
		// 		'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Officiis dolorum quo, soluta ullam deleniti tempora sint consequatur neque facere eligendi praesentium omnis libero, amet illum laudantium incidunt doloribus perspiciatis! Sit.'
		// 	],
		// 	[
		// 		'nama' => 'Sg Dua',
		// 		'gambar' => 'https://images.unsplash.com/photo-1612025710202-969463b1b834?ixid=MXwxMjA3fDB8MHxzZWFyY2h8NXx8cGVuYW5nfGVufDB8fDB8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60',
		// 		'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Officiis dolorum quo, soluta ullam deleniti tempora sint consequatur neque facere eligendi praesentium omnis libero, amet illum laudantium incidunt doloribus perspiciatis! Sit.'
		// 	]
		// ];

		 $db = db_connect();

		 $result = $db->query('SELECT * FROM gambar ORDER BY nama asc');
		 $all_pekan = $result->getResult();

		 //dd( $all_pekan);

		return view('homepage', ['all_pekan' => $all_pekan]);
	}

	function hello(){
		echo "<h1>Hello..</h1>";
	}

	function welcome(){
		echo "<h1>Welcome..</h1>";
	}
}
